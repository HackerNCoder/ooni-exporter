// Command oonifetch is a binary for fetching riseupvpn data over aws and
// parsing it into openmetrics format
//
package main

import (
	"log"

	ooniexporter "gitlab.torproject.org/hackerncoder/ooni-exporter/ooniexporter"
)

func main() {
	defer func() {
		if s := recover(); s != nil {
			log.Fatal(s)
		}
	}()
	ooniexporter.Main()
}
