package testutils

import "net/http"

// MockClient mock http.Client
type MockClient struct {
	// MockDo implements the mocking function
	MockDo func(req *http.Request) (*http.Response, error)
	// EndpointMap contains a map of URLs (keys) and the amount of times they have been called (values)
	// it can be used to implement different mocking behavior for multiple calls on the same same endpoint
	EndpointMap map[string]int
}

// NewMockClient initializes a new MockClient
func NewMockClient() *MockClient {
	mockClient := new(MockClient)
	mockClient.EndpointMap = map[string]int{}
	mockClient.MockDo = func(req *http.Request) (*http.Response, error) { return nil, nil }
	return mockClient
}

// Do matches the http.clients Do function signature and delegates to MockDo
func (m *MockClient) Do(req *http.Request) (*http.Response, error) {
	return m.MockDo(req)
}
