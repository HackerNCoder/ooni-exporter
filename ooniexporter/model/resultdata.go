package model

import (
	"encoding/json"

	"github.com/ooni/probe-engine/experiment/tor"
	probe "github.com/ooni/probe-engine/model"
)

// ResultData contains a collection of TestResults
type ResultData struct {
	testResults               map[string]TestResult
	testResultsCount          map[string]int
	countryResults            map[string][]string
	asnResults                map[string][]string
	TorConnections        map[string][]TorConnection // key - gateway IPs
}

// FailingTargetResult contains details about transport, port, ip
// of the failing gateway as well as the network and country the test failed in
type TorConnection struct {
	TargetResults     tor.TargetResults
	ProbeASN          string
	ProbeCC           string
	Occurences        int
}

//Equals compares two FailingGatewayConnection by it's struct values exept of the occurences field
func (f *TorConnection) Equals(gatewayConnection TorConnection) bool {
	return f.TargetResults.TargetAddress == gatewayConnection.TargetResults.TargetAddress &&
		f.TargetResults.TargetProtocol == gatewayConnection.TargetResults.TargetProtocol &&
		f.ProbeASN == gatewayConnection.ProbeASN &&
		f.ProbeCC == gatewayConnection.ProbeCC
}

// NewResultData instanciates a new ResultData struct
func NewResultData() *ResultData {
	result := new(ResultData)
	result.testResults = map[string]TestResult{}
	result.testResultsCount = map[string]int{}
	result.countryResults = map[string][]string{}
	result.asnResults = map[string][]string{}
	result.TorConnections = map[string][]TorConnection{}
	return result
}

// GetTestResultCount returns the sum of registered test results
func (r *ResultData) GetTestResultCount() int {
	counter := 0
	for _, i := range r.testResultsCount {
		counter = counter + i
	}
	return counter
}

// CountryForASN returns the country code for a given network identifier
func (r *ResultData) CountryForASN(asn string) string {
	if testHashes := r.asnResults[asn]; testHashes != nil {
		probeCC := r.testResults[testHashes[0]].ProbeCC
		return probeCC
	}
	return "n/a"
}

// TorConnectionsByAddressAndCountry returns a map containing IPs as keys and lists
// of failing connections sorted by countries as values
func (r *ResultData) TorConnectionsByAddressAndCountry() map[string][]TorConnection {
	results := map[string][]TorConnection{}
	for ip, failingConnections := range r.TorConnections {
		countryMap := map[string]TorConnection{}
		results[ip] = []TorConnection{}
		for _, connection := range failingConnections {
			if _, ok := countryMap[connection.ProbeCC]; !ok {
				connection.TargetResults.TargetProtocol = "n/a"
				connection.TargetResults.TargetAddress = "n/a"
				connection.ProbeASN = "n/a"
				countryMap[connection.ProbeCC] = connection
			} else {
				existingEntry := countryMap[connection.ProbeCC]
				existingEntry.Occurences = existingEntry.Occurences + connection.Occurences
				countryMap[connection.ProbeCC] = existingEntry
			}
		}
		for _, connection := range countryMap {
			results[ip] = append(results[ip], connection)
		}
	}
	return results
}

// TorConnectionsByAddressAndTransportAndCountry returns a map containing IPs as keys and lists
// of failing connections sorted by countries and transport types as values
func (r *ResultData) TorConnectionsByAddressAndTransportAndCountry() map[string][]TorConnection {
	results := map[string][]TorConnection{}
	for ip, failingConnections := range r.TorConnections {
		countryTransportMap := map[string]TorConnection{}
		results[ip] = []TorConnection{}
		for _, connection := range failingConnections {
			key := connection.ProbeCC + connection.TargetResults.TargetProtocol
			if _, ok := countryTransportMap[key]; !ok {
				connection.TargetResults.TargetAddress = "n/a"
				connection.ProbeASN = "n/a"
				countryTransportMap[key] = connection
			} else {
				existingEntry := countryTransportMap[key]
				existingEntry.Occurences = existingEntry.Occurences + connection.Occurences
				countryTransportMap[key] = existingEntry
			}
		}
		for _, connection := range countryTransportMap {
			results[ip] = append(results[ip], connection)
		}
	}
	return results
}

// FailingConnectionsByAddressAndNetwork returns a map containing IPs as keys and lists
// of failing connections sorted by network ASNs as values
func (r *ResultData) FailingConnectionsByAddressAndNetwork() map[string][]TorConnection {
	results := map[string][]TorConnection{}
	for ip, failingConnections := range r.TorConnections {
		networkMap := map[string]TorConnection{}
		results[ip] = []TorConnection{}
		for _, connection := range failingConnections {
			if _, ok := networkMap[connection.ProbeASN]; !ok {
				connection.TargetResults.TargetProtocol = "n/a"
				connection.TargetResults.TargetAddress = "n/a"
				networkMap[connection.ProbeASN] = connection
			} else {
				existingEntry := networkMap[connection.ProbeASN]
				existingEntry.Occurences = existingEntry.Occurences + connection.Occurences
				networkMap[connection.ProbeASN] = existingEntry
			}
		}
		for _, connection := range networkMap {
			results[ip] = append(results[ip], connection)
		}
	}
	return results
}

// FailingDirPortByCountry returns a map with a network identifier and the amount tests with at least one blocked Gateway
func (r *ResultData) FailingDirPortByCountry() map[string]int {
	return r.matchingTestsInMap(r.countryResults, failingDirPort)
}

// FailingDirPortByNetwork returns a map with a network identifier and the amount tests with at least one blocked Gateway
func (r *ResultData) FailingDirPortByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, failingDirPort)
}

// SuccessfulDirPortByCountry returns a map with a network identifier and the amount tests with no blocked Gateways
func (r *ResultData) SuccessfulDirPortByCountry() map[string]int {
	return r.matchingTestsInMap(r.countryResults, successfulDirPort)
}

// SuccessfulDirPortByNetwork returns a map with a network identifier and the amount tests with no blocked Gateways
func (r *ResultData) SuccessfulDirPortByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, successfulDirPort)
}

// FailingOBFS4ByCountry returns a map with a network identifier and the amount tests with a blocked API
func (r *ResultData) FailingOBFS4ByCountry() map[string]int {
	return r.matchingTestsInMap(r.countryResults, failingOBFS4)
}

// FailingOBFS4ByNetwork returns a map with a network identifier and the amount tests with a blocked API
func (r *ResultData) FailingOBFS4ByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, failingOBFS4)
}

// SuccessfulOBFS4ByCountry returns a map with a network identifier and the amount tests with an accessible API
func (r *ResultData) SuccessfulOBFS4ByCountry() map[string]int {
	return r.matchingTestsInMap(r.countryResults, successfulOBFS4)
}

// SuccessfulOBFS4ByNetwork returns a map with a network identifier and the amount tests with an accessible API
func (r *ResultData) SuccessfulOBFS4ByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, successfulOBFS4)
}

// FailingTestsByCountry returns a map with country codes and the amount of failing tests
func (r *ResultData) FailingTestsByCountry() map[string]int {
	return r.matchingTestsInMap(r.countryResults, failingTest)
}

// FailingTestsByNetwork returns a map with network codes and the amount of failing tests
func (r *ResultData) FailingTestsByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, failingTest)
}

// SuccessfulTestsByCountry returns a map with country codes and the amount of successful tests
func (r *ResultData) SuccessfulTestsByCountry() map[string]int {
	return r.matchingTestsInMap(r.countryResults, successfulTest)
}

// SuccessfulTestsByNetwork returns a map with country codes and the amount of successful tests
func (r *ResultData) SuccessfulTestsByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, successfulTest)
}

// AddMeasurement adds a test result, counts the occurences of the same result
// and saves references to the country and network used
func (r *ResultData) AddMeasurement(measurement probe.Measurement) {
	result := parseMeaseurement(measurement)
	r.addTestResult(result)
}

func successfulTest(test TestResult) bool {
	return successfulDirPort(test) && successfulOBFS4(test) && successfulORPortDirauth(test)
}
func failingTest(test TestResult) bool {
	return failingDirPort(test) || failingOBFS4(test) || failingORPortDirauth(test)
}

func failingOBFS4(test TestResult) bool {
	return test.OBFS4Accessible != test.OBFS4Total
}
func successfulOBFS4(test TestResult) bool {
	return test.OBFS4Accessible == test.OBFS4Total
}

func failingDirPort(test TestResult) bool {
	return test.DirPortAccessible < 5
}
func successfulDirPort(test TestResult) bool {
	return test.DirPortAccessible > 4
}

func failingORPortDirauth(test TestResult) bool {
	return test.ORPortDirauthAccessible < 5
}
func successfulORPortDirauth(test TestResult) bool {
	return test.ORPortDirauthAccessible > 4
}

func (r *ResultData) matchingTestsInMap(bucket map[string][]string, condition func(TestResult) bool) map[string]int {
	matchingTests := map[string]int{}
	for key, testHashes := range bucket {
		counter := 0
		for _, testHash := range testHashes {
			test := r.testResults[testHash]
			if condition(test) {
				counter++
			}
		}
		matchingTests[key] = counter
	}
	return matchingTests
}

func parseMeaseurement(measurement probe.Measurement) TestResult {
	tk := getTorTestKeys(measurement)
	result := TestResult{
		EngineName:      measurement.Annotations["engine_name"],
		EngineVersion:   measurement.Annotations["engine_version"],
		ProbeCC:         measurement.ProbeCC,
		ProbeASN:        measurement.ProbeASN,
		ResolverASN:     measurement.ResolverASN,
		ReportID:        measurement.ReportID,
		DirPortAccessible: tk.DirPortAccessible,
		DirPortTotal: tk.DirPortTotal,
		OBFS4Accessible: tk.OBFS4Accessible,
		OBFS4Total: tk.OBFS4Total,
		ORPortDirauthAccessible: tk.ORPortDirauthAccessible,
		ORPortDirauthTotal: tk.ORPortDirauthAccessible,
		Targets: tk.Targets,
	}
	return result
}

func getTorTestKeys(measurement probe.Measurement) tor.TestKeys {
	testKeysInterface := measurement.TestKeys
	// convert map to json
	jsonString, _ := json.Marshal(testKeysInterface)
	// convert json to struct
	tk := tor.TestKeys{}
	json.Unmarshal(jsonString, &tk)
	return tk
}

// AddTestResult adds a test result, counts the occurences of the same result
// and saves references to the country and network used
func (r *ResultData) addTestResult(result TestResult) {
	hash := result.Hash()
	//log.Println("adding result: " + fmt.Sprint(result))
	if r.testResultsCount[hash] == 0 {
		r.testResults[hash] = result
	}
	r.testResultsCount[hash] = r.testResultsCount[hash] + 1
	probeCC := result.ProbeCC
	if _, ok := r.countryResults[probeCC]; !ok {
		//log.Println("creating a new map for " + probeCC)
		r.countryResults[probeCC] = []string{}
	}
	r.countryResults[probeCC] = append(r.countryResults[probeCC], hash)
	network := result.ProbeASN
	if _, ok := r.asnResults[network]; !ok {
		//log.Println("creating a new map for " + network)
		r.asnResults[network] = []string{}
	}
	r.asnResults[network] = append(r.asnResults[network], hash)

	for _, gateway := range result.Targets {
		address := gateway.TargetAddress
		target := TorConnection{
			TargetResults: gateway,
			Occurences:        1,
			ProbeCC:           result.ProbeCC,
			ProbeASN:          result.ProbeASN,
		}

		// create a new array of FailingGatewayConnections if there's not yet a connection stored
		// with same IP
		if _, ok := r.TorConnections[address]; !ok {
			r.TorConnections[address] = []TorConnection{}
		}

		// search for entries with same IP, Port, Transport, ASN and CC and
		// increase the occourences counter if an entry is already there
		found := false
		for i, connection := range r.TorConnections[address] {
			if target.Equals(connection) {
				found = true
				connection.Occurences = connection.Occurences + 1
				r.TorConnections[address][i] = connection
				break
			}
		}

		// add current FailingGatewayConnection object to the list if there's no one yet for the given ip
		if !found {
			r.TorConnections[address] = append(r.TorConnections[address], target)
		}
	}
}
