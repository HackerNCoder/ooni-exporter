package model

import (
	"crypto"
	"fmt"

	"github.com/ooni/probe-engine/experiment/tor"
)

// TestResult contains the main details of an experiment
type TestResult struct {
	EngineName      string
	EngineVersion   string
	ProbeCC         string
	ResolverASN     string
	ProbeASN        string
	ReportID        string
	DirPortTotal            int64
	DirPortAccessible       int64
	OBFS4Total              int64
	OBFS4Accessible         int64
	ORPortDirauthTotal      int64
	ORPortDirauthAccessible int64
	Targets         map[string]tor.TargetResults
}

// Hash generates a hash over the country code and the experiment stati of a TestResult
func (t *TestResult) Hash() string {
	digester := crypto.MD5.New()

	fmt.Fprint(digester, t.ProbeCC)
	fmt.Fprint(digester, t.DirPortAccessible)
	fmt.Fprint(digester, t.OBFS4Accessible)
	fmt.Fprint(digester, t.ORPortDirauthAccessible)
	fmt.Fprint(digester, t.Targets)

	return string(digester.Sum(nil))
}
