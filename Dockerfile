FROM debian:buster-backports AS build
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        build-essential pkg-config git ca-certificates \
&& DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends -t buster-backports golang-go \
&& rm -rf /var/lib/apt/lists/*

ENV GOPATH=/go
WORKDIR $GOPATH
RUN git clone https://0xacab.org/leap/ooni-exporter /ooni-exporter && cd /ooni-exporter && go build
RUN strip /ooni-exporter/ooni-exporter

FROM registry.git.autistici.org/ai3/docker/chaperone-base
COPY --from=build /ooni-exporter/ooni-exporter /usr/local/bin/ooni-exporter
COPY chaperone.d/ /etc/chaperone.d

ENTRYPOINT ["/usr/local/bin/chaperone"]
